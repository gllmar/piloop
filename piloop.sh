#!/bin/bash
export DISPLAY=:0
sleep 1
xrandr --addmode HDMI-2 1920x1080
xrandr --output HDMI-1 --mode 480x800 --pos 0x0 --rotate left --scale 2.4x2.25  --output HDMI-2 --mode 1920x1080 --pos 0x0
sleep 3
cvlc --fullscreen --input-repeat=999999999 /home/pi/Realia_I.mp4

