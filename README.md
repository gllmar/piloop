# piloop

pi loop player at boot on both hdmi


## Setup

Config txt


```
[HDMI:0]
   hdmi_group=1
   hdmi_mode=16
   hdmi_force_hotplug=1

 [HDMI:1]
   hdmi_group=1
   hdmi_mode=16
   hdmi_force_hotplug=1
   
```

## xorg

HDMI 1 (0) -> 480x800

HDMI 2 (1) -> 1920x1080
